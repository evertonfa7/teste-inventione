// Seus scripts aqui
const checkBoxElement = document.getElementById("enable-second-panel");
const secondSection = document.getElementsByClassName("second").item(0);
const formElement = document.getElementsByClassName("form-wrapper").item(0);

checkBoxElement.addEventListener("change", showPanel);
formElement.addEventListener("submit", handleSubmit);

function showPanel(event) {
  event.preventDefault();

  if (this.checked) {
    secondSection.classList.remove("hide");
  } else {
    secondSection.classList.add("hide");
  }
}

function handleSubmit(event) {
  event.preventDefault();

  const nameInput = document.getElementById("name").value;
  const age = document.getElementById("age").value;
  const date = document.getElementById("date").value;
  const genderM = document.getElementById("male");
  const genderF = document.getElementById("female");
  const textareaElement = document.getElementById("text-area");

  let numberOfWords = wordCounter(textareaElement.value);

  let genderSelect;
  if (genderM.checked) {
    genderSelect = "Masculino";
  } else if (genderF.checked) {
    genderSelect = "Feminino";
  }

  let dateFormated = new Date(date)
    .toISOString()
    .match(/^[0-9]{4}\-[0-9]{2}\-[0-9]{2}/)[0]
    .split("-")
    .reverse()
    .join("/");

  const layout = `
    <h3>Dados: </h3>
    <p>Nome: ${nameInput}</p>
    <p>Idade: ${age} anos</p>
    <p>Sexo: ${genderSelect}</p>
    <p>Data: ${dateFormated}</p>
    <p>Total de palavras: ${numberOfWords}</p>
  `;

  const result = document.getElementById("result");
  result.style.display = "block";
  result.innerHTML = layout;
}

function wordCounter(text) {
  let arrWords = text.split(" ");
  let wordCount = 0;
  for (let i = 0; i < arrWords.length; i++) {
    if (isWord(arrWords[i])) {
      wordCount++;
    }
  }
  return wordCount;
}

function isWord(str) {
  // verifica se são espaços
  if (str.trim().length === 0) return false;

  // verifica se são consoantes isoladas
  if (!hasVowel(str)) return false;

  // percorre por cada char da string e obtem o seu código
  for (let index = 0; index < str.length; index++) {
    let code = str.charCodeAt(index);
    // verifica se o char da string não está em a-z ou A-Z
    if (!((code > 64 && code < 91) || (code > 96 && code < 123))) {
      // sinais graficos permitos no começo e final da palavra
      if (index === 0 || index === str.length - 1) {
        continue;
      }
      // ignora o hifem no meio da palavra (caso de palavra composta)
      if (String.fromCharCode(code) === "-") {
        continue;
      }
      return false;
    }
  }

  return true;
}

function hasVowel(x) {
  return /[aeiouAEIOU]/.test(x);
}

function datetime() {
  var title = document.getElementById("title");
  var now = new Date();

  title.innerHTML = title.innerHTML + " (" + now.toLocaleTimeString() + ")";
}

datetime();
