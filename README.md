# Desafio Invnetione

Descrição
- **Qual parte do código escrito deveria ser prioritariamente testada e por quê?** <br/>
A área de texto (textarea) que tem como funcionalidade a contagem das palavras, deve ser prioritariamente testada pois foi feita uma simples implementação com javascript puro sem nenhum uso de biblioteca externa. Sendo assim, dependendo do caso, se o texto não estiver bem formatado essa funcionalidade pode apresentar inconsistências na contagem das palavras.

- **Quais ferramentas poderiam ser utilizadas para testar a parte do código sugerida?** <br/>
Poderiam ser utilizados ferramentas de testes unitários e funcionais, como por exemplo: JsUnit, Jasmini, Jest ou Mocha.

- **Qual estratégia de teste você considera ideal para ser utilizada neste cenário?** <br/>
O ideal para essa funcionalidade seria a utilização de alguma biblioteca como o JQuery ou solução mais complexa utilizando Regex. 
Para o teste dessa funcionalidade seria ideal a criação de teste unitário e talvez uma automação com Selenium.


# Ajuda

??
